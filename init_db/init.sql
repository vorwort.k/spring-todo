CREATE TABLE task_users (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL, /** ユーザー名 */
  email VARCHAR(255) NOT NULL,
  CONSTRAINT upst_email UNIQUE(email)
);
INSERT INTO task_users (name, email) values ('test1', 'test@test1');
INSERT INTO task_users (name, email) values ('test2', 'test@test2');
INSERT INTO task_users (name, email) values ('test3', 'test@test3');
CREATE TABLE chat (
  name  VARCHAR,
  message VARCHAR,
  updated_time timestamp
);
CREATE TYPE status_enum AS ENUM (
    '新規',
    '対応中',
    '完了',
    '重要'
);
ALTER TYPE status_enum OWNER TO postgres;
CREATE TABLE task (
  id SERIAL PRIMARY KEY,
  title  VARCHAR,
  message VARCHAR,
  updated_time timestamp,
  updated_user_id SERIAL NOT NULL,
  status status_enum,
  member_id SERIAL NOT NULL,
  FOREIGN KEY (updated_user_id) REFERENCES task_users(id),
  FOREIGN KEY (member_id) REFERENCES task_users(id)
);

COMMENT ON COLUMN task.status IS '新規、対応中、完了、重要';
-- INSERT INTO task (title,updated_user_id, member_id) values ('AAA',1, 2);
-- INSERT INTO task (title,updated_user_id, member_id) values ('BBB',2, 3);
-- INSERT INTO task (title,updated_user_id, member_id) values ('CCC',3, 1);
-- INSERT INTO users (name, status) values ('test', 'テスト');
-- INSERT INTO users (name, status) values ('wada-puyo', 'ぷよぷよ');
-- INSERT INTO users (name, status) values ('wada-puni', 'ぷにぷに');
CREATE ROLE testuser WITH LOGIN PASSWORD 'testpass';
GRANT SELECT,UPDATE,INSERT,DELETE ON ALL TABLES IN SCHEMA public TO testuser;