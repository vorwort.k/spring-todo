var Vue = new Vue({
    el: "#app",
    data: {
        isModalActive: false,
        modalTitle: "タイトルテスト",
        modalDescription: "タスクの詳細のサンプル",
        todoList: null,
        taskId: 1,
        userList: null,
        modalUserId: 1
    },
    methods: {
        selectTask: function () {
            axios
                .get('/select_task')
                .then(response => (this.todoList = response.data));
        },
        updateModal: function (taskId, modalTitle, modalDescription, modalUserId) {
            this.taskId = taskId;
            this.modalTitle = modalTitle;
            this.modalDescription = modalDescription;
            this.modalUserId = modalUserId;
            this.isModalActive = true;
        },
        insertModal: function () {
            this.taskId = 0;
            this.modalTitle = 'タイトルを入力';
            this.modalDescription = 'タスクの詳細';
            this.modalUserId = 1;
            this.isModalActive = true;
        },
        selectUser: function () {
            axios
                .get('/select_user')
                .then(response => (this.userList = response.data));
        },
        initUser: function () {
            axios
                .get('/init_user')
                .then(response => (this.userList = response.data));
        },
        statusFilter: function (status = null) {
            const vueThis = this;
            axios
                .get('/select_task')
                .then(function (response) {
                    const allList = response.data;
                    vueThis.todoList = null;
                    vueThis.todoList = allList.filter(function (item, index) {
                        if (item.status == status || !status) return true;
                    });
                })
        },
    },
    mounted: function () {
        this.selectTask();
        this.selectUser();
        this.initUser();
    }
})
