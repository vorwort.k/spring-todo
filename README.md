# 概要

以下を用いたアプリケーションの作成  
- Docker 
- Java: Spring
  - Gradle
- DB: Postgres

# 動作方法

## docker-composeで起動する

バックグラウンドで`app`を起動します。

```
docker-compose up -d app
```

データベースを初期の状態にしたい場合は`-v`をつけます

```
docker-compose up -d -V db
```

# ホットデプロイ

ローカルで`gradle`のビルドを行うことでホットデプロイすることができます。`JDK14`で動作させているので`JDK14`をインストールしておいてください。

## JDK14の準備

[JDK14のダウンロード](http://jdk.java.net/14/)

[環境変数の設定などはこちら](https://www.javadrive.jp/start/install/index1.html#section1)

## ローカルのGradleで継続的にビルドする

ローカルのGradleでインプットのファイル（ソースのファイル）を監視して変更があればビルドします。

```
cd app
./gradlew build -t
```

## ChromeにLiveReloadプラグインを追加する

コンテナで静的なファイルに変更があった場合、LiveReloadサーバーが`35729`がポートフォワーディングされているので、以下のプラグインを追加することで、自動的にリロードされる。

[LiveReload](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei)

## Google 認証における設定

`docker-compose.yml`と同じ階層に`.env`を作成する。
作成した`.env`に以下の設定を行う。

```
GOOGLE_CLIENT_ID=誰かに教えてもらってください
GOOGLE_CLIENT_SECRET=誰かに教えてもらってください
```